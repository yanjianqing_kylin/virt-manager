%global __python %{__python3}

Name:                virt-manager
Version:             4.1.0
Release:             8
Summary:             The manage virtual machines tool which via libvirt.
License:             GPLv2+
BuildArch:           noarch
URL:                 https://virt-manager.org/
Source0:             https://virt-manager.org/download/sources/virt-manager/virt-manager-%{version}.tar.gz

Patch1:              Add-loongarch-support.patch
Patch2:              Add-loongarch-support-in-guest-class.patch
Patch3:              Add-some-default-device-support-for-loongarch.patch
Patch4:              Add-test-cases-for-loongarch.patch
Patch5:              Update-chinese-translation-file.patch
Patch6:              backport-cli-disk-Add-driver.metadata_cache-options.patch
Patch7:              backport-tests-cli-Fix-test-output-after-previous-commit.patch
Patch8:              backport-fsdetails-Fix-an-error-with-source.socket-of-virtiof.patch
Patch9:              backport-cli-Drop-unnecessary-disk-prop-aliases.patch
Patch10:             backport-tests-testdriver-Add-filesystem-socket-example.patch
Patch11:             backport-virtinstall-split-no_install-conditional-apart-to-tr.patch
Patch12:             backport-virtinstall-fix-regression-with-boot-and-no-install-.patch
Patch13:             backport-tests-Add-a-compat-check-for-linux2020-in-amd-sev-te.patch
Patch14:             backport-cli-cpu-Add-maxphysaddr.-mode-bits-options.patch
Patch15:             backport-virt-install-help-required-options-are-wrong.patch
Patch16:             backport-cloner-Sync-uuid-and-sysinfo-system-uuid.patch
Patch17:             backport-virt-install-unattended-and-cloud-init-conflict.patch
Patch18:             backport-virt-install-Reuse-cli.fail_conflicting.patch
Patch19:             backport-cli-support-boot-loader.stateless.patch
Patch20:             backport-diskbackend-Drop-support-for-sheepdog.patch

Requires:            virt-manager-common = %{version}-%{release} python3-gobject gtk3 libvirt-glib >= 0.0.9
Requires:            gtk-vnc2 dconf vte291 gtksourceview4
Recommends:          (libvirt-daemon-kvm or libvirt-daemon-qemu) libvirt-daemon-config-network
BuildRequires:       git gettext python3-devel python3-docutils
Suggests:            python3-libguestfs


%description
The virtual machine management tool uses libvirt as the management API and provides
graphical tools for managing KVM,Xen and LXC.Used to start, stop, add or delete
virtual devices, Connect to the console via graphics or serial to view and count the
resource usage and provide it to the virtual machine.

%package             common
Summary:             Files used for Virtual Machine Manager interfaces
Requires:            python3-argcomplete python3-libvirt python3-libxml2 python3-requests
Requires:            libosinfo >= 0.2.10 python3-gobject-base xorriso

%description         common
The files used by virt-manager interfaces, as virt-install related tools.

%package -n virt-install
Summary:             Utilities for installing virtual machines
Requires:            virt-manager-common = %{version}-%{release} libvirt-client
Provides:            virt-install virt-clone virt-xml

%description -n virt-install
Package provides several command line utilities, including virt-clone (clone an
existing virtual machine) and virt-install (build and install new VMs).

%package             help
Summary:             Documentation for user of virt-manager.

%description         help
Documentation for user of virt-manager.

%prep
%autosetup -n virt-manager-%{version} -p1

%build
./setup.py configure --default-hvs "qemu,xen,lxc" --default-graphics=vnc

%install
./setup.py --no-update-icon-cache --no-compile-schemas install -O1 --root=%{buildroot}
%find_lang virt-manager
for f in $(find %{buildroot} -type f -executable -print); do
    sed -i "1 s|^#!/usr/bin/env python3|#!%{__python3}|" $f || :
done

%if 0%{?py_byte_compile:1}
%py_byte_compile %{__python3} %{buildroot}%{_datadir}/virt-manager/
%endif

%files
%doc README.md COPYING NEWS.md
%{_bindir}/virt-manager
%{_datadir}/virt-manager/ui/*.ui
%{_datadir}/virt-manager/{virt-manager,virtManager,icons}
%{_datadir}/icons/hicolor/*/apps/*
%{_datadir}/applications/virt-manager.desktop
%{_datadir}/glib-2.0/schemas/org.virt-manager.virt-manager.gschema.xml
%{_datadir}/metainfo/virt-manager.appdata.xml

%files common -f virt-manager.lang
%dir %{_datadir}/virt-manager
%{_datadir}/virt-manager/{virtcli,virtconv,virtinst}

%files -n virt-install
%{_datadir}/bash-completion/completions/{virt-install,virt-clone,virt-convert,virt-xml}
%{_bindir}/{virt-install,virt-clone,virt-xml}

%files               help
%{_mandir}/man1/virt-manager.1*
%{_mandir}/man1/{virt-install.1*,virt-clone.1*,virt-xml.1*}

%changelog
* Wed Aug 21 2024 yanjianqing <yanjianqing@kylinos.cn> - 4.1.0-8
- diskbackend: Drop support for sheepdog
- cli: support --boot loader.stateless=
- virt-install: Reuse cli.fail_conflicting
- virt-install: --unattended and --cloud-init conflict
- cloner: Sync and system uuid
- virt-install: --help required options are wrong
- cli: --cpu: Add maxphysaddr.{mode,bits} options
- tests: Add a compat check for linux2020 in amd-sev test case
- virtinstall: fix regression with --boot and no install method
- virtinstall: split no_install conditional apart to track code coverage

* Fri Jul 26 2024 yanjianqing <yanjianqing@kylinos.cn> - 4.1.0-7
- tests: testdriver: Add filesystem socket example
- cli: Drop unnecessary --disk prop aliases
- fsdetails: Fix an error with source.socket of virtiofs
- tests: cli: Fix test output after previous commit
- cli: --disk: Add driver.metadata_cache options

* Wed Jul 17 2024 yanjianqing <yanjianqing@kylinos.cn> - 4.1.0-6
- Update  chinese translation file

* Thu Feb 29 2024 yanjianqing <yanjianqing@kylinos.cn> - 4.1.0-5
- delete Fix-bug-that-virt-manager-can-not-support-dies.patch,virt-manager-4.1.0 support dies

* Thu Feb 29 2024 lixianglai <lixianglai@loongson.cn> - 4.1.0-4
- Update loongarch code

* Wed Feb 28 2024 lijunwei <lijunwei@kylinos.cn> - 4.1.0-3
* fix bug that virt-manager can not support features dies 

* Tue Jan 10 2023 zhaotianrui <zhaotianrui@loongson.cn> - 4.1.0-2
- Add loongarch support

* Thu Sep 08 2022 wangdi <wangdi@kylinos.cn> - 4.1.0-1
- Upgrade to 4.1.0

* Mon May 30 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 3.2.0-1
- Upgrade to 3.2.0

* Thu Feb 17 2022 liuxingxiang <liuxingxiang@kylinsec.com.cn> - 2.1.0-6
- Improve simplified Chinese translation

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 2.1.0-5
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Mon May 31 2021 huanghaitao <huanghaitao8@huawei.com> - 2.1.0-4
- Completing build dependencies to fix git command missing error

* Wed Apr 22 2020 Jeffery.Gao <gaojianxing@huawei.com> - 2.1.0-3
- Package init
